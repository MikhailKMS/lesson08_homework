import ru.sber.jd.MyStringHashMap;

public class Main {

    public static void main(String[] args) {

        MyStringHashMap myStringHashMap = new MyStringHashMap();

        myStringHashMap.put(1, "FirstValue");
        myStringHashMap.put(2, "SecondValue");
        myStringHashMap.put(20, "20Value");
        myStringHashMap.put(24, "24Value");
        myStringHashMap.put(21, "21Value");
        myStringHashMap.put(22, "22Value");
        myStringHashMap.put(23, "23Value");

        myStringHashMap.put(1, "New_FirstValue");
        myStringHashMap.put(1, "New2_FirstValue");

        myStringHashMap.put(20, "New_20Value");
        myStringHashMap.put(24, "New_24Value");


        System.out.println(myStringHashMap);

        System.out.println("Количество элементов: " + myStringHashMap.size());

        System.out.println("\nЗначение элемента HashMap по ключу 24 = " + myStringHashMap.get(24));
        System.out.println("\nКлюч элемента HashMap для значения 'New_24Value' = " + myStringHashMap.containValue("New_24Value"));

    }


}
